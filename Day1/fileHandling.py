file=open("basic.txt",'r')
print(file.read())
print(file.read()) # nothing got printed as pointer is at the end of file
file.seek(0) # internal pointer at starting
data=file.read()
print(data)
file.seek(0)
print(file.readlines())
file.seek(0)
print(file.readline())
file.close()
file=open("abc.rmg",'w');
file.write("Hello welcome to this beautiful place.");
file.close()
file=open("abc.rmg",'r')
print(file.readline())
file.close()
file=open("abc.rmg",'a')
file.write("Adding more content")
file.close()
file=open("abc.rmg")
print(file.readlines())
file.close()