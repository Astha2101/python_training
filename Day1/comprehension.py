str="hello"
strlst=[]
for letter in str:
    strlst.append(letter)
print(strlst)
l1=[letter for letter in str]
print(l1)
l2=[x for x in "I Love India!!"]
print(l2)
l3=[num for num in range(0,11)]
print(l3)
l4=[x**2 for x in range(0,20) if x%2==0]
print(l4)
celcius=[0,10,0.35,40]
fahrenheit=[((9/5)*temp+32) for temp in celcius]
print(fahrenheit)
l5=[x if x%2==0 else "Odd" for x in range(0,11)]
print(l5)
l6=[x*y for x in [1,2,3] for y in [100,200,300]]
print(l6)

