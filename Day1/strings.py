str1="Welcome to strings in python !!"
print(str1)
print(type(str1))
print(len(str1))
str2="God is great"
print(str2[0])
print(str2[5])
print(str2[-1])
print(str2[2:])
print(str2[:3])
print(str2[4:6])
print(str2)
print(str2[::2])
print(str2)
print(str2[::-1])
str3="Hello World"
print(str3)
temp=" Kitty"
str3=str3[:5]+temp
print(str3)
str4='2'+'3'
print(str4)
str5='a'*10
print(str5)
print(str3.upper())
print(str3.capitalize())
print(str3.split(" "))
print(str3.find("K"))
print(str3.replace(str3,"Hello World!"))
print("My name is {}".format("Alley"))
name="Alley"
print(f"My name is {name}")
print("I like {},{},{}".format("Apple","Watermelon","Orange"))
print("I like {w}, {o}, {a}".format(a="apple",w="watermelon",o="orange"))
result=100/777
print(f"Result is {result}")
print(f"Result is {result:1.2f}")
print("I am %i years old."% 22)
print("I like %s"%"apples")