# data structure and objects assessment
#1. given a string "hello" , write index command to print 'e'
str="hello"
print(str[1])

#2. reverse the string using slicing
str="Hello World"
print(str[::-1])

#3. buid a list with [0,0,0] in two ways
l1=[0,0,0]
print(type(l1))
print(l1)
l2=[]
l2.append(0)
l2.append(0)
l2.append(0)
print(l2)
 
""" 4. Reassign in 'hello' in the following list to "goodbye" 
        [1,2,[3,4,"hello"]]"""
lst=[1,2,[3,4,"hello"]]
print(lst)
lst[2][2]="goodbye"
print(lst)

""" 5. using keys, indexing grab "hello" from following dicitonaries:
    i. d={"simple_key":"hello"} """
d1={"simple_key":"hello"}
print(d1["simple_key"])

#ii. d={"k1":{"k2":"hello"}}
d2={"k1":{"k2":"hello"}}
print(d2['k1']['k2'])

#iii. d={"k1":[1,2,{'k2':['this is tricky',{'tough':[1,2,"hello"]}]}]}
d3={"k1":[1,2,{'k2':['this is tricky',{'tough':[1,2,"hello"]}]}]}
print(d3['k1'][2]['k2'][1]['tough'][2])



