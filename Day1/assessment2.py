
""" 1. Use for, .split(), and if to create a Statement that will print out words that start with 's':
               st = 'Print only the words that start with s in this sentence'"""

st = "Print only the words that start with s in this sentence"
lst=st.split(" ")
listOfWords=[]
for i in lst:
    if(i[0]=='s'): 
        listOfWords.append(i)
print(listOfWords)

# 2.Use range() to print all the even numbers from 0 to 10.

for i in range(0,10):
    if(i%2==0):
        print(i)

# 3. Use a List Comprehension to create a list of all numbers between 1 and 50 that are divisible by 3.

l1=[x for x in range(1,50) if x%3==0]
print(l1)

"""4. Go through the string below and if the length of a word is even print "even!"
            st = 'Print every word in this sentence that has an even number of letters' """

st = 'Print every word in this sentence that has an even number of letters'
for words in st.split():
    if len(words)%2==0:
        print(words)

"""5. Write a program that prints the integers from 1 to 100. But for multiples of three print "Fizz" instead of the number, and for the multiples of five print "Buzz". For numbers which are multiples of both three and five print "FizzBuzz". """

for i in range(1,101):
    if(i%3==0 and i%5==0): print("FizzBuzz")
    elif(i%3==0): print("Fizz")
    elif(i%5==0): print("Buzz")
    else: print(i)

"""6. Use List Comprehension to create a list of the first letters of every word in the string below:
          st = 'Create a list of the first letters of every word in this string' """

st = 'Create a list of the first letters of every word in this string'
l2=[x[0] for x in st.split()]
print(l2)

