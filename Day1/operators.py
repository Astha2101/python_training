# range function
lst=[11,23,43,54,11,26,76]
for i in range(len(lst)):
    print(lst[i])
# enumerate function
index_count=0
word="abcde"
for letter in word:
    print(word[index_count])
    index_count+=1
for index,letter in enumerate(word):
    print(index)
    print(letter)
    print("\n")

# zip function
l1=[1,2,3]
l2=['a','b','c']
for item in zip(l1,l2):
    print(item)

# in operator
print('x' in l2)
print('k1' in {"k1":1})

# min and max function
print(min(l1))
print(max(l2))

# shuffle function
from random import randint, shuffle
lst=[1,1,2,2,2,3,3,4,4,4,4,5,5,6,6,6,7,7,9]
shuffle(lst)
print(lst)
lt=shuffle(lst)
print(type(lt))
print(lt)

# random function
from random import randint
print(randint(0,100))
num=randint(100,1000)
print(num)

