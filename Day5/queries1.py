import mysql.connector

mydb=mysql.connector.connect(
    host='localhost',
    user='root',
    password='21011999',
    database='studentdb'
)

myCursor=mydb.cursor()

# Inserting the values in table in different ways

sql = "INSERT INTO student (name, address) VALUES (%s, %s)"
val = ("John", "Highway 21")
myCursor.execute(sql, val)
mydb.commit()
print(myCursor.rowcount, "record inserted.")


sql = "INSERT INTO student (name, address) VALUES (%s, %s)"
val = [
  ('Peter', 'Lowstreet 4'),
  ('Amy', 'Apple st 652'),
  ('Hannah', 'Mountain 21'),
  ('Michael', 'Valley 345'),
  ('Sandy', 'Ocean blvd 2'),
  ('Betty', 'Green Grass 1'),
  ('Richard', 'Sky st 331'),
  ('Susan', 'One way 98'),
  ('Vicky', 'Yellow Garden 2'),
  ('Ben', 'Park Lane 38'),
  ('William', 'Central st 954'),
  ('Chuck', 'Main Road 989'),
  ('Viola', 'Sideway 1633')
]
myCursor.executemany(sql, val)
mydb.commit()


sql = "INSERT INTO student (name, address) VALUES (%s, %s)"
val = ("Michelle", "Blue Village")
myCursor.execute(sql, val)
mydb.commit()
print("1 record inserted, Roll Number:", myCursor.lastrowid)

# Retreiving data from table

myCursor.execute("SELECT * FROM student")
myresult = myCursor.fetchall()
for x in myresult:
  print(x)

myCursor.execute("SELECT roll_number,name, address FROM student")
myresult = myCursor.fetchall()
for x in myresult:
  print(x)

myCursor.execute("SELECT * FROM student")
myresult = myCursor.fetchone()
print(myresult)

# updating data in tables

sql = "UPDATE student SET address = 'Canyon 123' WHERE address = 'Blue Village'"
myCursor.execute(sql)
mydb.commit()
print(myCursor.rowcount, "record(s) affected")