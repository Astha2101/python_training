import csv

data=open('example.csv',encoding='utf-8')
csv_data=csv.reader(data)
dataLines=list(csv_data)
print(dataLines[0])
print(len(dataLines))
for line in dataLines[:5]:
    print(line)


file_to_output=open('to_save_csv.csv',mode='w',newline='')
csv_writer=csv.writer(file_to_output,delimiter=',')
csv_writer.writerow(['a','b','c'])
csv_writer.writerows([[1,2,3],['q','w','r'],[4,5,6]])
file_to_output.close()
