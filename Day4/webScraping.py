import requests
import bs4

result=requests.get("http://www.example.com")
print(type(result))
print(result.text)

soup=bs4.BeautifulSoup(result.text,'lxml')
print(soup)   # to print the text as it is

# grabbing text

print(soup.select('title'))
print(soup.select('h1'))
print(soup.select('p'))
print(soup.select('p')[0].getText())

# grabbing a class

req=requests.get("https://en.wikipedia.org/wiki/Grace_Hopper")
soup=bs4.BeautifulSoup(req.text,'lxml')
print(soup)

print(soup.select(".toctext"))
first_item=soup.select(".toctext")[0]
print(first_item.text)
for item in soup.select(".toctext"):
    print(item.text)

# grabbing an image

reqt=requests.get("https://en.wikipedia.org/wiki/Deep_Blue_(chess_computer)")
soup=bs4.BeautifulSoup(reqt.text,"lxml")
print(soup.select("img"))
print(soup.select("img")[0])
print(soup.select(".thumbimage"))

picture=soup.select(".thumbimage")[0]
print(type(picture))
print(picture['src'])

image_link=requests.get('https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Kasparov_Magath_1985_Hamburg-2.png/220px-Kasparov_Magath_1985_Hamburg-2.png')
print(image_link.content)

f=open('my_image.jpg','wb')
f.write(image_link.content)
f.close()





