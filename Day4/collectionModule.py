from collections import Counter

lst=[1,1,1,1,2,2,2,2,3,3,4,4,4,4,5,5,6]
print(Counter(lst))
mt=Counter(lst)
print(mt)
print(type(mt))
print(mt.most_common())
print(Counter('aaaaabbbbccdddeeeeffghiijk'))

from collections import defaultdict

d=defaultdict(lambda:-1)
d['correct']=1
print(d['correct'])
print(d['Wrong key!']) # instead of raising error , it will print the default value

from collections import namedtuple

Dog=namedtuple('Dog',['age','breed','name']) # a class named dog is created
jimmy=Dog('2','German Shephard','Jimmy')
print(jimmy)
print(type(jimmy))
print(jimmy.name)
print(jimmy.age)
print(jimmy.breed)
print(jimmy[0])



