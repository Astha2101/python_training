import random

suits=('Hearts','Clubs','Spades','Diamonds')
ranks=('Two','Three','Four','Five','Six','Seven','Eight','Nine','Ten','Jack','Queen','King','Ace')
values={'Two':2,'Three':3,'Four':4,'Five':5,'Six':6,'Seven':7,'Eight':8,'Nine':9,'Ten':10,'Jack':11,'Queen':12,'King':13,'Ace':14}

class Card():
    def __init__(self,suit,rank):
        self.suit=suit.capitalize()
        self.rank=rank.capitalize()
        self.value=values[self.rank]
    
    def __str__(self):
        return self.rank+" of "+self.suit

class Deck():
    def __init__(self):
        self.allCards=[]
        for suit in suits:
            for rank in ranks:
                newCard=Card(suit,rank)
                self.allCards.append(newCard)
    
    def shuffle(self):
        random.shuffle(self.allCards)

    def deal_one(self):
        return self.allCards.pop()


class Player():
    def __init__(self,name):
        self.name=name
        self.allCards=[]
    
    def removeOne(self):
        return self.allCards.pop(0)

    def addCard(self,newCard):
        if type(newCard)==type([]):
            self.allCards.extend(newCard)
        else:
            self.allCards.append(newCard)

playerOne=Player("One")
playerTwo=Player("Two")

newDeck=Deck()
newDeck.shuffle()

for i in range(26):
    playerOne.addCard(newDeck.deal_one())
    playerTwo.addCard(newDeck.deal_one())

game_on=True
round_no=0

while game_on:
    round_no+=1
    print(f"Round : {round_no}")

    if len(playerOne.allCards)==0:
        print("Player 1 out of cards !! Player 2 wins.")
        game_on=False
        break
    if len(playerTwo.allCards)==0:
        print("Player 2 out of cards !! Player 1 wins.")
        game_on=False
        break

    playerOneCards=[]
    playerOneCards.append(playerOne.removeOne())

    playerTwoCards=[]
    playerTwoCards.append(playerTwo.removeOne())

    at_war=True
    while at_war:
        if playerOneCards[-1].value>playerTwoCards[-1].value:
            playerOne.addCard(playerOneCards)
            playerOne.addCard(playerTwoCards)
            at_war=False
        elif playerTwoCards[-1].value>playerOneCards[-1].value:
            playerTwo.addCard(playerTwoCards)
            playerTwo.addCard(playerOneCards)
            at_war=False
        else:
            print('WAR!')
            if len(playerOne.allCards)<5:
                print("Player one unable to play at war. Game over at war!!")
                print("Player 2 won the match!")
                game_on=False
                break
            elif len(playerTwo.allCards)<5:
                print("Player two unable to play at war. Game over at war!!")
                print("Player 1 won the match!")
                game_on=False
                break
            else:
                for num in range(5):
                    playerOneCards.append(playerOne.removeOne())
                    playerTwoCards.append(playerTwo.removeOne())



