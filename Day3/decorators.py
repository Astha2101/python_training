def new_decorator(original_function):
    def wrap_function():
        print("Some code added before executing original function")
        original_function()
        print("Some code added after executing original function")
    return wrap_function

def func_required_decorator():
    print("Hi I need to be decorated.")

decorated_func=new_decorator(func_required_decorator)
decorated_func()

@new_decorator
def new_func():
    print("Hi I am a decorated function.")

new_func()


