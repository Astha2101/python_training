# Handle the exception thrown by the code below by using try and except blocks.

try:
    for i in ['a','b','c']:
        print(i**2)
except:
    print("Power is not applicable for str")

# Handle the exception thrown by the code below by using try and except blocks. Then use a finally block to print 'All Done.'

x = 5
y = 0
try:
    z = x/y
except ZeroDivisionError:
    print('Cannot divide by zero!')
finally:
    print('All done.')

# Write a function that asks for an integer and prints the square of it. Use a while loop with a try, except, else block to account for incorrect inputs.

def ask():
    while True:
        try:
            num=int(input("Enter a number :"))
        except:
            print("Please enter a int value!")
            continue;
        else:
            print(f'Square of {num} is {num**2}')
            break;
        finally:
            print("Thank you!!")
ask()


