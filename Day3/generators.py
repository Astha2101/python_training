# create a generator that generates the squares of numbers up to some number N.
def square_generator(N):
    for x in range(N):
        yield x**2

print(type(square_generator(10)))
print(list(square_generator(10)))

for x in square_generator(10):
    print(x)

sg=square_generator(3)
print(next(sg))
print(next(sg))
print(next(sg))

# Create a generator that yields "n" random numbers between a low and high number (that are inputs).

import random
def generate_random(n,low,high):
    for x in range(n):
        yield random.randint(low,high)

print(list(generate_random(10,0,100)))

# Use the iter() function to convert the string below into an iterator:
s="hello"
s_iterable=iter(s)
print(next(s_iterable))
print(next(s_iterable))
print(next(s_iterable))
print(next(s_iterable))
print(next(s_iterable))

# comprehension:- iterating over while performing some task

my_list = [1,2,3,4,5]

gencomp = (item for item in my_list if item > 3)
for item in gencomp:
    print(item)
