# Fill in the Line class methods to accept coordinates as a pair of tuples and return the slope and distance of the line.

class Line:
    
    def __init__(self,coor1,coor2):
        self.coor1=coor1
        self.coor2=coor2
    
    def distance(self):
        return ((self.coor1[0]-self.coor2[0])**2+(self.coor1[1]-self.coor2[1])**2)**0.5
    
    def slope(self):
        return (self.coor2[1]-self.coor1[1])/(self.coor2[0]-self.coor1[0])
c1=(3,4)
c2=(8,10)
l=Line(c1,c2)
print(l.distance())
print(l.slope())

# Fill in the class

class Cylinder:
    
    def __init__(self,height=1,radius=1):
        self.height=height
        self.radius=radius
        
    def volume(self):
        return (3.14*self.radius**2*self.height)
    
    def surface_area(self):
        return (2*3.14*self.radius*(self.radius+self.height))
c=Cylinder(2,3)
print(c.volume())
print(c.surface_area())

        