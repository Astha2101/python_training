import random

currentPlayer=0
def displayBoard(board):
    print('\n'*5)
    print('   |   |')
    print(' ' + board[7] + ' | ' + board[8] + ' | ' + board[9])
    print('   |   |')
    print('-----------')
    print('   |   |')
    print(' ' + board[4] + ' | ' + board[5] + ' | ' + board[6])
    print('   |   |')
    print('-----------')
    print('   |   |')
    print(' ' + board[1] + ' | ' + board[2] + ' | ' + board[3])
    print('   |   |')

def playerInput():
    value=''
    while not(value=='X' or value=='O'):
        value=input("Player 1: Do you want to X or O?").upper()
    if value=='X': return ('X','O')
    return ('O','X')

def placeValue(board,value,position):
    board[position]=value

def winCheck(board,value):
    if board[7]==board[8]==board[9]==value: return True
    elif board[7]==board[4]==board[1]==value: return True
    elif board[8]==board[5]==board[2]==value: return True
    elif board[9]==board[6]==board[3]==value: return True
    elif board[4]==board[5]==board[6]==value: return True
    elif board[1]==board[2]==board[3]==value: return True
    elif board[1]==board[5]==board[9]==value: return True
    elif board[3]==board[5]==board[7]==value: return True
    return False

def chooseFirst():
    if random.randint(0,1)==0:
        return 'Player 2'
    else: return 'Player 1'

def spaceCheck(board,position):
    return board[position]==' '

def fullBoardCheck(board):
    for i in range(1,10):
        if board[i]==' ': return False
    return True


def playerChoice(board):
    position=0
    while position not in range(1,10) or not spaceCheck(board,position):
        pos=input("Player "+str(currentPlayer)+" choose your next position (1-9) ")
        if len(pos)>1: continue
        position=int(pos)
    return position

def replay():
    return input("Do you want to play again? Enter Yes or No:  ").lower().startswith('y')

print('Welcome to the game of Tic Tac Toe !!!')
while True:
    board=[' ']*10
    player1Value,player2Value=playerInput()    
    turn=chooseFirst()
    print(turn+' will go first.')

    play_game=input("Are you ready to play? Yes or No: ")
    if play_game.lower()[0]=='y':
        game_on=True
    else: game_on=False

    while game_on:
        if turn=="Player 1":
            currentPlayer=1
            displayBoard(board)
            position=playerChoice(board)
            placeValue(board,player1Value,position)
            if winCheck(board,player1Value):
                displayBoard(board)
                print("Congratulations! Player 1 won the game!!")
                game_on=False
            else:
                if fullBoardCheck(board):
                    displayBoard(board)
                    print("The game is a draw!!")
                    game_on=False
                else: turn='Player 2'
        else:
            currentPlayer=2
            displayBoard(board)
            position=playerChoice(board)
            placeValue(board,player2Value,position)
            if winCheck(board,player2Value):
                displayBoard(board)
                print("Congratulations! Player 2 won the game!!")
                game_on=False
            else:
                if fullBoardCheck(board):
                    displayBoard(board)
                    print("The game is a draw!!")
                    game_on=False
                else: turn='Player 1'
    if not replay():
        break


                
