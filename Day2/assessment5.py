""" For this challenge, create a bank account class that has two attributes:

owner
balance
and two methods:

deposit
withdraw
As an added requirement, withdrawals may not exceed the available balance.

Instantiate your class, make several deposits and withdrawals, and test to make sure the account can't be overdrawn. """

class BankAccount():
    def __init__(self,owner,balance):
        self.owner=owner
        self.balance=balance
    
    def deposit(self,amount):
        self.balance+=amount
        print("Amount deposited!")

    def withdraw(self,amount):
        if amount>self.balance: print("Insufficient balance !!")
        else:
            self.balance-=amount
            print("Amount withdrawn!")
    def __str__(self):
        return (f"Account owner: {self.owner}\nAccount balance: {self.balance}")

account=BankAccount("Jose",1000)
print(account)
account.deposit(500)
account.withdraw(2000)
account.withdraw(200)