def square(num):
    return num**2
lst=[1,2,3,4,5]
for i in map(square,lst):
    print(i)
print(list(map(lambda num: num**2,lst)))

def checkEven(num):
    if num%2==0: return True
    else: return False
lst=[1,2,3,4,5,6,7,8,9,10]
print(list(filter(checkEven,lst)))
print(list(filter(lambda num: num%2==0,lst)))

names=['Andy','Bella','Casey']
print(list(map(lambda x:x[::-1],names)))

