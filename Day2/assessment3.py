"""  Write a function that returns the lesser of two given numbers if both numbers are even, but returns the greater if one or both numbers are odd¶ """
def func1(a,b):
    if a%2==0 and b%2==0:
        return min(a,b)
    else: return max(a,b)
print(func1(2,4))
print(func1(3,7))
print(func1(5,8))
print(func1(10,10))

""" Write a function takes a two-word string and returns True if both words begin with same letter """
def func2(str):
    lst=str.split()
    return lst[0][0]==lst[1][0]
print(func2('Levelheaded Llama'))
print(func2('Crazy Kangaroo'))

""" Given two integers, return True if the sum of the integers is 20 or if one of the integers is 20. If not, return False"""
def func3(a,b):
    if(a==20 or b==20): return True
    elif(a+b==20): return True
    else: return False
print(func3(20,10))
print(func3(12,8))
print(func3(2,3))

""" Write a function that capitalizes the first and fourth letters of a name"""
def func4(str):
    st=''
    if len(str)>3: 
        st=str[0].upper()+str[1:3]+str[3].upper()+str[4:]
        return st
    else:
        return "Name is too short !"
print(func4("abrahim"))
print(func4("Tom"))

""" Given a sentence, return a sentence with the words reversed"""
def func5(sentence):
    return ' '.join(sentence.split()[::-1])
print(func5("I am home"))
print(func5("It's 5:00 am right now."))

""" Given an integer n, return True if n is within 10 of either 100 or 200"""
def func6(num):
    if num>=91 and num<=110 or num>=191 and num<=210: return True
    else: return False
print(func6(97))
print(func6(50))
print(func6(209))
""" Given a list of ints, return True if the array contains a 3 next to a 3 somewhere."""
def func7(lst):
    for i in range(len(lst)-1):
        if lst[i]==3 and lst[i+1]==3: return True
    return False
print(func7([1,2,3,3]))
print(func7([3,1,3]))
print(func7([1,2,3]))

""" Given a string, return a string where for every character in the original there are three characters"""
def func8(str):
    st=''
    for c in str:
        st+=3*c
    return st
print(func8("Hello"))
print(func8("World!"))

""" Given three integers between 1 and 11, if their sum is less than or equal to 21, return their sum. If their sum exceeds 21 and there's an eleven, reduce the total sum by 10. 
        Finally, if the sum (even after adjustment) exceeds 21, return 'BUST' """
def func9(a,b,c):
    sum=a+b+c
    if sum<=21: return str(sum)
    elif 11 in (a,b,c): return str(sum-10)
    else: return "BUST"
print(func9(5,6,7))
print(func9(9,9,9))
print(func9(9,9,11))
 


