Python can be used in database applications.

One of the most popular NoSQL database is MongoDB.

MongoDB
MongoDB stores data in JSON-like documents, which makes the database very flexible and scalable.

PyMongo
Python needs a MongoDB driver to access the MongoDB database.

Dowload PyMongo: pip install PyMongo

Test PyMongo
To test if the installation was successful, or if you already have "pymongo" installed, create a Python page with the following content:

demo_mongodb_test.py:

import pymongo
