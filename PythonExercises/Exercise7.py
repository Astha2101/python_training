tabby_cat = "\tI'm tabbed in."
persian_cat = "I'm split\non a line."
backslash_cat = "I'm \\ a \\ cat."

fat_cat = """
I'll do a list:
\t* Cat food
\t* Fishies
\t* Catnip\n\t* Grass
""" 

print (tabby_cat)
print (persian_cat)
print (backslash_cat)
print (fat_cat)

#Use ''' (triple-single-quote) instead. Can you see why you might use that instead of """?

fat_cat = '''
I'll do a list:
\t* Cat food
\t* Fishies
\t* Catnip\n\t* Grass
'''
print(fat_cat)

#Escape Sequences.
#\\ Backslash (\)
#\' Single-quote (') \" Double-quote (")
#\a ASCII bell (BEL)
#\b ASCII backspace (BS)
#\f ASCII formfeed (FF)
#\n ASCII linefeed (LF)
#\N{name} Character named name in the Unicode database (Unicode only) 
#\r ASCII carriage return (CR)
#\t ASCII horizontal tab (TAB) 

