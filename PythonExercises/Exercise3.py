my_name = 'Zed A. Shaw'
my_age=35#notalie
my_height = 74 # inches
my_weight = 180 # lbs
my_eyes = 'Blue'
my_teeth = 'White'
my_hair = 'Brown'

print ("Let's talk about %s." % my_name)
print ("He's %d inches tall." % my_height)
print ("He's %d pounds heavy." % my_weight)
print ("Actually that's not too heavy.")
print ("He's got %s eyes and %s hair." %(my_eyes, my_hair))
print ("His teeth are usually %s depending on the coffee." % my_teeth)

# this line is tricky, try to get it exactly right
print("If I add %d,%d,and %d I get %d."%(my_age, my_height, my_weight, my_age + my_height + my_weight))

# Try more format characters. %r is a very useful one. It’s like saying “print this no matter what.”
print ("Let's talk about %r." % my_name)
print ("He's %r inches tall." % my_height)

# Try to write some variables that convert the inches and pounds to centimeters and kilos. Do not just type in the measurements. Work out the math in Python.
my_height_cms=my_height*2.54
my_weight_kgs=my_weight/2.205
print ("He's %d cms tall." % my_height_cms)
print ("He's %d kgs heavy." % my_weight_kgs)