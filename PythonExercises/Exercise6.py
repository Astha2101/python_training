formatter = "%r %r %r %r"

print (formatter % (1, 2, 3, 4))
print (formatter % ("one", "two", "three", "four"))
print (formatter % (True, False, False, True))
print (formatter % (formatter, formatter, formatter, formatter)) 
print (formatter % (
'I had this thing.',
'That you could type up right.', "But it didn't sing.",
"So I said goodnight."))

"""
Notice that the last line of output uses both single-quotes and double-quotes for indi- vidual pieces. 
Why do you think that is?
"""
# It uses double-quote in 3rd statement as the string already consist a single-quote so it will
# create ambiguity , that's why it is printed in double-quote.

print ("""
There's something going on here.
With the three double-quotes.
We'll be able to type as much as we like.
Even4linesifwewant,or5,or6.
""")



